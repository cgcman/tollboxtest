package grdj.com.toolboxtest.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
//import com.mediatechindo.wahyu.materialuikit.tools.ImageViewCircleTransform;

import java.util.ArrayList;
import java.util.List;

import grdj.com.toolboxtest.listeners.GalleryClickListener;
import grdj.com.toolboxtest.models.ImageModel;
import grdj.com.toolboxtest.ImageViewCircleTransform;
import grdj.com.toolboxtest.R;
import grdj.com.toolboxtest.globals.Configs;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class GalleryAdapter1 extends RecyclerView.Adapter<GalleryAdapter1.ItemViewHolder> {
    private static List<ImageModel> dataList=new ArrayList<>();
    private LayoutInflater mInflater;
    private Context context;
    private GalleryClickListener clicklistener = null;

    public GalleryAdapter1(Context ctx, List<ImageModel> data) {
        context = ctx;
        dataList = data;
        mInflater = LayoutInflater.from(context);
        Log.d("IMAGENES",dataList.toString());
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView image;
        private TextView title;

        public ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            image = (ImageView) itemView.findViewById(R.id.image);
            title= (TextView) itemView.findViewById(R.id.bigTitle);
        }

        @Override
        public void onClick(View v) {

            if (clicklistener != null) {
                clicklistener.itemClicked1(v, getAdapterPosition());
            }
        }
    }

    public void setClickListener(GalleryClickListener listener) {
        this.clicklistener = listener;
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_gallery_big, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.title.setText(dataList.get(position).getTitle());
        Glide.with(context)
                .load(Configs.get_img_url() + dataList.get(position).getImageUrl())
                .transform(new ImageViewCircleTransform(context))
                .thumbnail(0.01f)
                .centerCrop()
                .into(holder.image);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
