package grdj.com.toolboxtest.listeners;

import android.view.View;

public interface GalleryClickListener {
    void itemClicked1(View view, int position);
    void itemClicked2(View view, int position);
}