package grdj.com.toolboxtest.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Cgcman on 10/06/2017.
 */

public class GalleryModel {
    public GalleryModel(){
    }

    private String id;
    private String title;
    private String type;
    private List<ImageModel> items = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String title) {
        this.type = type;
    }

    public List<ImageModel> getImages() {
        return items;
    }

    public void setImages(List<ImageModel> items) {
        this.items = items;
    }


}
