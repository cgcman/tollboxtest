package grdj.com.toolboxtest.models;

/**
 * Created by Wahyu on 06/08/2015.
 */
public class ImageModel {

    private String id;
    private String url;
    private String title;
    private String video;

    public ImageModel(){
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return url;
    }

    public void setImageUrl(String imageUrl) {
        this.url = imageUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String title) {
        this.video = video;
    }

}
