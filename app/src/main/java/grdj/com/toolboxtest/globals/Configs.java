package grdj.com.toolboxtest.globals;

/**
 * Created by Cgcman on 10/06/2017.
 */
public class Configs {
    private static Configs ourInstance = new Configs();
    private static String IMAGE_URL="http://placeimg.com/";

    public static Configs getInstance() {
        return ourInstance;
    }

    private Configs() {
    }

    public void set_img_url(String url){
        IMAGE_URL=url;
    }

    public static String get_img_url(){
        return IMAGE_URL;
    }
}
