package grdj.com.toolboxtest.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.github.rtoshiro.view.video.FullscreenVideoLayout;

import java.io.IOException;

import grdj.com.toolboxtest.R;

/**
 * Created by Cgcman on 13/06/2017.
 */

public class VideoActivity extends AppCompatActivity {

    private FullscreenVideoLayout videoLayout;
    private Uri videoUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_layout);

        videoLayout = (FullscreenVideoLayout) findViewById(R.id.videoview);
        videoLayout.setActivity(this);

        Uri videoUri = Uri.parse("http://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4");
        try {
            videoLayout.setVideoURI(videoUri);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
