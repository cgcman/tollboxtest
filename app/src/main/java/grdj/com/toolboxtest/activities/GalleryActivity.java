package grdj.com.toolboxtest.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import grdj.com.toolboxtest.listeners.GalleryClickListener;
import grdj.com.toolboxtest.models.GalleryModel;
import grdj.com.toolboxtest.models.ImageModel;
import grdj.com.toolboxtest.R;
import grdj.com.toolboxtest.adapters.GalleryAdapter1;
import grdj.com.toolboxtest.adapters.GalleryAdapter2;
import grdj.com.toolboxtest.rest.RestGallerys;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GalleryActivity extends AppCompatActivity implements GalleryClickListener,View.OnClickListener{

    private List<GalleryModel> data= new ArrayList<>();
    private List<ImageModel> rowListItem1 = new ArrayList<>();
    private List<ImageModel> rowListItem2 = new ArrayList<>();
    private GalleryAdapter1 rcAdapter1;
    private GalleryAdapter2 rcAdapter2;
    private RecyclerView rView1;
    private RecyclerView rView2;
    private Context cntx;
    private TextView photoCount1;
    private TextView photoCount2;
    private Intent _videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_layout);
        cntx=this;

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        // ADD SPACE TOP DRAWER ON LOLLIPOP AND UP
        final NavigationView navigationViewLeft = (NavigationView) findViewById(R.id.nav_view);
        View navLeftLay = navigationViewLeft.getHeaderView(0);
        Space spaceLeftTop = (Space) navLeftLay.findViewById(R.id.spaceLeftTop);
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            spaceLeftTop.setVisibility(View.VISIBLE);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //ArrayList<ImageModel> rowListItem1 = getAllItemList1();
        //ArrayList<ImageModel> rowListItem2 = getAllItemList2();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        //LinearLayoutManager layoutManager3 = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);

        rView1 = (RecyclerView) findViewById(R.id.recyclerViewLandscape);
        rView2 = (RecyclerView) findViewById(R.id.recyclerViewUrbanCity);

        rView1.setHasFixedSize(true);
        rView1.setLayoutManager(layoutManager1);
        rView1.setNestedScrollingEnabled(false);
        rView1.setHasFixedSize(false);

        rView2.setHasFixedSize(true);
        rView2.setLayoutManager(layoutManager2);
        rView2.setNestedScrollingEnabled(false);
        rView2.setHasFixedSize(false);

        photoCount1= (TextView) findViewById(R.id.photoCount1);
        photoCount2= (TextView) findViewById(R.id.photoCount2);

        loadJSON();

    }

    private void loadJSON(){
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.grdj.com.ar/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        RestGallerys restClient = retrofit.create(RestGallerys.class);
        Call<List<GalleryModel>> call = restClient.getData();

        call.enqueue(new Callback<List<GalleryModel>>() {

            @Override
            public void onResponse(Call<List<GalleryModel>> call, Response<List<GalleryModel>> response) {
                switch (response.code()) {
                    case 200:
                        data = response.body();
                        Log.d("DATA","DATA: "+data.get(0).getImages());
                        rowListItem1=data.get(0).getImages();
                        rowListItem2=data.get(1).getImages();
                        photoCount1.setText(data.get(0).getImages().size()+" Imagenes");
                        photoCount2.setText(data.get(1).getImages().size()+" Imagenes");
                        rcAdapter1 = new GalleryAdapter1(cntx, rowListItem1);
                        rcAdapter2 = new GalleryAdapter2(cntx, rowListItem2);
                        rView1.setAdapter(rcAdapter1);
                        rView2.setAdapter(rcAdapter2);
                        rcAdapter1.setClickListener((GalleryClickListener) cntx);
                        rcAdapter2.setClickListener((GalleryClickListener) cntx);
                        //spinner.setVisibility(View.GONE);
                        break;
                    case 401:
                        Log.d("DATA","DATA: 401");
                        break;
                    default:
                        Log.d("DATA","DATA: default");
                        break;
                }
            }

            @Override
            public void onFailure(Call<List<GalleryModel>> call, Throwable t) {
                Log.e("error", t.toString());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.loginsignup_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
           /* case R.id.action_search:
                Toast.makeText(this, "action search clicked!", Toast.LENGTH_SHORT).show();
                break;
            case R.id.action_settings:
                Toast.makeText(this, "action setting clicked!", Toast.LENGTH_SHORT).show();
                break;*/
            default:
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            /*case R.id.btnLoginSignupBack:
                onBackPressed();
                break;*/
            default:
                break;
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public void itemClicked1(View view, int position) {
        //int num = position + 1;
        //Toast.makeText(this, "List 1 Position " + num, Toast.LENGTH_SHORT).show();
        _videoView= new Intent(GalleryActivity.this,VideoActivity.class);
        startActivity(_videoView);

    }

    @Override
    public void itemClicked2(View view, int position) {
        //int num = position + 1;
        //Toast.makeText(this, "List 2 Position " + num, Toast.LENGTH_SHORT).show();
        _videoView= new Intent(GalleryActivity.this,VideoActivity.class);
        startActivity(_videoView);
    }
}
