package grdj.com.toolboxtest.rest;

import java.util.List;

import grdj.com.toolboxtest.models.GalleryModel;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Cgcman on 08/05/2017.
 */

public interface RestGallerys {
    @GET("data.json")
    Call<List<GalleryModel>> getData();
}